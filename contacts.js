
import {commandArray} from "./commandArray.js";
import addContact from "./addContact.js";
import {contacts} from "./contactsDataModel.js";
import {readContacts, saveContacts} from "./dbFileIO.js";
import listContacts from "./listContacts.js";
import modifyContacts from "./modifyContacts.js";
import modifyGroup from "./modifyGroup.js";
import deleteContacts from "./deleteContacts.js";
import searchContact from "./searchContact.js";

function sorted(a, b) {
    if(a.lastname < b.lastname) return -1;
    if(a.lastname > b.lastname) return 1;
    return 0;
}

function sortContacts() { // lajittelu sukunimen mukaan aakkosjärjestyksessä
    const getcontacts = readContacts();   
    const sortedlist = getcontacts.sort(sorted);
    listContacts(sortedlist);
}

const komennot = () => "\n" +commandArray().reduce((acc, value) => {
    return acc += `'${value.mark}'(${value.text})\n`;
}, "");

function main(command) {
    const commandInfo = "Yhteystietosovellus. Käyttö 'node contacts.js <komento>. " + komennot();

    if(!command) {
        console.log(commandInfo);
        process.exit(1);
    } 
    switch (command) {
        case "q": //quit program
            process.exit(1);
            break;
        case "a": //add contact
            addContact(); 
            break;
        case "l": //list contacts
            listContacts();
            break;
        case "s": //sort contacts
            sortContacts();
            break;
        case "m": //modify contacts
            modifyContacts();
            break;
        case "d": //delete contacts
            deleteContacts();
            break;
        case "w": //test: write example json
            saveContacts(contacts);
            break;
        case "mg": //modify contact group
            modifyGroup(); 
            break;
        case "f":
            searchContact();
            break;
        case "r": //test: read contacts
            console.log(readContacts()); 
            break;
        case "h":
            console.log(commandInfo);
            process.exit(0);
            break;
        default:
            console.log(`Received: ${command}. Tuntematon komento`);
            process.exit(0);
            break;
    }
}

const args = process.argv; 
const command = args[2];
main(command);
