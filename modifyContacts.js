export default modifyContacts;
import {rl, readContacts, saveContacts, getGroup} from "./dbFileIO.js";

function modifyContacts() {
    const contact = {
        "firstname": null, 
        "lastname": null, 
        "phone": [], 
        "email": [],
        "street": null,
        "notes": null,
        "group": null
    };

    const content = readContacts();

    let index;
    let n = 0;
    const GROUP = 8;
    const END = 7;

    const recursiveAsyncReadLine = function () {
        if (n === END)
        {
            changeOne(content, index, contact);
            console.log("Bye!");
            process.exit(1);
        }
        else if (n === 0)
        {
            rl.question("Kirjoita Etunimi Sukunimi: ( Poistu = 'q'): ", input => {
                if(input === "q") {
                    process.exit(1);
                }
                contact.firstname = input.split(" ")[0];
                contact.lastname = input.split(" ")[1];
                index = findContact(contact, content);
                if (index) {
                    console.log(content[index]);
                    n++;
                    recursiveAsyncReadLine();
                }
                else 
                    recursiveAsyncReadLine();
                //return rl.close(); //closing RL and returning from function.
            });
        }
        else if (n === 1) {
            rl.question("Etunimi? ", input => {
                contact.firstname = input;
                n++;
                recursiveAsyncReadLine();
            });
        }
        else if (n === 2){
            rl.question("Sukunimi? ", input => {
                contact.lastname = input;
                n++;
                recursiveAsyncReadLine();
            });
        }
        else if (n === 3){
            rl.question("phone? ", input => {
                const phoneArr = input.split(" ");
                phoneArr.forEach(number => {
                    contact.phone.push(number);
                });
                n++;
                recursiveAsyncReadLine();
            });
        }
        else if (n === 4){
            rl.question("email? ", input => {
                const emailArr = input.split(" ");
                emailArr.forEach(email => {
                    contact.email.push(email); 
                });
                n++;
                recursiveAsyncReadLine();
            });
        }
        else if (n === 5){
            rl.question("street? ", input => {
                contact.street = input;
                n++;
                recursiveAsyncReadLine();
            });
        }
        else if (n === 6){
            rl.question("notes? ", input => {
                contact.notes = input;
                n = GROUP;
                recursiveAsyncReadLine();
            });
        }
        else if (n === GROUP) {
            rl.question("group? ", input => {
                contact.group = getGroup(input);
                n = END;
                recursiveAsyncReadLine();
            });
        }
    };
    recursiveAsyncReadLine();
}

function findContact(contact, content) {
    let index;
    content.forEach(function(element, i) {
        if ((element.firstname === contact.firstname)&&(element.lastname === contact.lastname)) {
            index = i;
        }
    });
    if(index) {
        return index;
    }
    else console.log(`'${contact.firstname} ${contact.lastname}' ei löydy. Yritä uudelleen.`);
    return false;
}

function changeOne(content, index, contact) {
    content[index] = contact;
    console.log("changed!");
    saveContacts(content);
    return false;
}
