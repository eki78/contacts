export default modifyGroup; 
import {readGroups, getGroup, readContacts, rl, saveContacts} from "./dbFileIO.js";

function modifyGroup() {
    const groups = readGroups();
    console.log("All groups:");
    groups.forEach(element => {
        console.log(element.name);
    });
    
    const kentat = ["phone", "email", "street","notes", "group"];
    const content = readContacts();

    let kentta;
    let groupId;
    let n = 0;
    
    const recursiveAsyncReadLine = function () {
        if (n === 3)
        {
            console.log("Bye!");
            return rl.close(); //closing RL and returning from function.
        }
        else if (n === 0)
        {
            rl.question("Kirjoita ryhmän nimi: ( Poistu = 'q'): ", input => {
                if(input === "q") {
                    process.exit(1);
                }
                groupId = groups.reduce((acc, obj) => {
                    if(obj.name === input) { //found correct group
                        return acc = obj.id;
                    } 
                    return acc;
                }, 0);
                if (!groupId) {
                    console.log(`Ryhmä '${input}' ei löytytnyt.`);
                    recursiveAsyncReadLine();
                }
                else
                {
                    const groupContacts = findGroupContacts(groupId, content);
                    console.table(groupContacts);
                    if (groupContacts.length > 0) 
                    {
                        n++;
                        recursiveAsyncReadLine();
                    }
                    else recursiveAsyncReadLine();
                }
            });
        }
        else if (n === 1) {
            rl.question("Mitä kenttää haluat muuttaa: phone, email, street, notes, group? ", input => {
                kentta = input;
                
                if (kentat.includes(kentta)) 
                {
                    n++;
                    recursiveAsyncReadLine();
                }
                else {
                    console.log(`Kenttä '${kentta}' ei löydy!`);
                    recursiveAsyncReadLine();
                }
            });
        }
        else if (n === 2) {
            rl.question(`uusi ${kentta}: `, input => {
                if (kentta === "phone")
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element.phone = input;
                            console.log("changed!");
                        }
                    });
                if (kentta === "email")
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element.email = input;
                            console.log("changed!");
                        }
                    });
                if (kentta === "street")
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element.street = input;
                            console.log("changed!");
                        }
                    });
                if (kentta === "notes")
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element.notes = input;
                            console.log("changed!");
                        }
                    });
                if (kentta === "group")
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element.group = getGroup(input);
                            console.log("changed!");
                        }
                    });
                saveContacts(content);
                
                n++;
                recursiveAsyncReadLine();
            });
        }
    };
    recursiveAsyncReadLine();

}

function findGroupContacts(groupId, content) {
    const groupContacts = content. filter(contact => contact.group === groupId);
    if(groupContacts.length > 0) {
        return groupContacts;
    }
    else console.log("Ryhmän yhteystietoja ei löydy");
    return false;
}
