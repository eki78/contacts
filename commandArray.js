export const commandArray = () => {
    return [{
        mark: "q",
        text: "poistu"
    },
    {
        mark: "a",
        text: "lisää"
    },
    {
        mark: "f",
        text: "etsi"
    },
    {
        mark: "l",
        text: "luettelo"
    },
    {
        mark: "s",
        text: "lajittelu"
    },
    {
        mark: "m",
        text: "muokkaa"
    },
    {
        mark: "d",
        text: "poista"
    },
    {
        mark: "mg",
        text: "muokkaa ryhmää"
    },
    {
        mark: "h",
        text: "komentoluettelo"
    }];
};