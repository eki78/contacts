import * as fs from "fs";
import * as readline from "node:readline";

export const rl = readline.createInterface({
    input: process.stdin, 
    output: process.stdout, 
    prompt: ">"
});


/**
 * Lukee yhteystiedot contacts.json -tiedostosta
 * @returns Array of contact objects
 */
export function readContacts() {
    let data; 
    try {
        data = fs.readFileSync("contacts.json", "utf-8");    
    } catch (error) { //no file was found
        data = "[]";
    }
    const contacts = JSON.parse(data); 
    return contacts; 
}

/**
 * Lukee ryhmät groups.json -tiedostosta
 * @returns Array of group objects
 */
export function readGroups() {
    let data; 
    try {
        data = fs.readFileSync("groups.json", "utf-8");
    } catch (error) { //no file was found
        data = "[]";
    }
    const groups = JSON.parse(data); 
    return groups; 
}


/**
 * Writes contacts.json file
 * @param {Array} content Array of objects to be saved
 */
export function saveContacts(content) {
    fs.writeFileSync("contacts.json", JSON.stringify(content), (err) => {
        if(err) console.log(err); 
        else console.log("wrote output");
    });
}


/**
 * Writes groups.json file
 * @param {Array} content Array of object to be saved
 */
export function saveGroups(content) {
    fs.writeFileSync("groups.json", JSON.stringify(content), (err) => {
        if(err) console.log(err); 
        else console.log("wrote output");
    });
}


/**
 * Hakee ryhmistä ryhmän tai luo uuden, jos ryhmää ei löydy. 
 * @param {String} input 
 */
export function getGroup(input) {
    const name = input.trim();
    if(name === "") {
        return 0;
    }
    //a. try to find group from existing groups
    const groups = readGroups();
    const groupId = groups.reduce((acc, obj) => {
        if(obj.name === name) { //found correct group
            return acc = obj.id;
        } 
        return acc;
    }, 0);
    //b. group didn't exist, create group
    if(groupId === 0) {
        const maxId = groups.reduce((acc, obj) => {
            if(obj.id > acc) {
                return obj.id;
            } else {
                return acc;
            }
        }, 1);
        const newGroup = {
            name,
            id: (parseInt(maxId)+1)
        };
        groups.push(newGroup); 
        saveGroups(groups);

        return newGroup.id;
    } 
    return groupId; 
}