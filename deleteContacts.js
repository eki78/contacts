
export default deleteContacts; 
import {readContacts, rl, saveContacts} from "./dbFileIO.js";

function deleteContacts() {
    const content = readContacts();
    console.table(content);
    if(content.length === 0) {
        console.log("Ei poistettavia yhteystietoja");
    } else {
        rl.question("Anna poistettavan indexinumero (Poistu = 'q'): ", input => {
            if(input === "q") {
                process.exit(0);
            } else if(input.length < 1 || isNaN(input)) {
                deleteContacts();
            } else {
                rl.question(`Poistetaanko indexi.nro ${input} ${content[input].firstname} ${content[input].lastname} (Kyllä/Ei)?: ` , answer => {
                    if(answer.toLocaleLowerCase() === "kyllä") {
                        console.log("Poistettu!");
                        content.splice(input, 1);
                        saveContacts(content);
                        deleteContacts();
                    } else {
                        deleteContacts();
                    }
                });
            }
        });
    }
}       
