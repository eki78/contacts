
export default searchContact;
import {rl, readContacts} from "./dbFileIO.js";

function finder(input) {
    input = input[0].toUpperCase()+input.slice(1);
    const content = readContacts();
    return content.filter(obj => Object.entries(obj).some(entry => entry.includes(input)));
}

function searchContact() {
    rl.question("Anna Etunimi tai Sukunimi: ( Poistu = 'q'): ", input => {
        if(input.length < 1) {
            searchContact();
        } else {
            const foundContacts = finder(input);
            if(input === "q") {
                process.exit(1);
            } else if(foundContacts.length > 0) {
                console.table(foundContacts);
                searchContact();
            } else {
                console.log("Ei löytynyt!");
                searchContact();
            }
        }
    });
}
