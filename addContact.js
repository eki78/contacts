/* eslint-disable no-fallthrough */
import {rl, readContacts, getGroup, saveContacts} from "./dbFileIO.js";
import {contacts} from "./contactsDataModel.js";
export default addContact;

function addContact() { 

    const newContact = {
        "firstname": null, 
        "lastname": null, 
        "phone": [], 
        "email": [],
        "notes": null,
        "street": null,
        "group": null
    };

    let localcontacts = readContacts(); 
    if(!localcontacts) {
        localcontacts = contacts;
    }

    let question = "firstname"; 
    console.log("Etunimi? ");
    rl.prompt();

    rl.on("line", input => {
        const emailArr = input.split(" ");
        const phoneArr = input.split(" ");
        switch(question) { //mikä oli kysymys
            case "firstname": 
                newContact.firstname = input; 
                question = "lastname";
                console.log("Sukunimi?");
                rl.prompt();
                break;
            case "lastname": 
                newContact.lastname = input; 
                question = "group";
                console.log("Ryhmä?");
                rl.prompt();
                break;
            case "group":
                newContact.group = getGroup(input);
                question = "email";
                console.log("Email osoitteet erotettuna välilyönnillä: ");
                break;
            case "email":
                emailArr.forEach(email => {
                    newContact.email.push(email); 
                });
                question = "notes";
                console.log("Muistiinpanot?");
                rl.prompt();
                break;
            case "notes": 
                newContact.notes = input;
                question = "phone"; 
                console.log("Puhelinnumerot erotettuna välilyönnillä: ");
                rl.prompt();
                break;
            case "phone": 
                
                phoneArr.forEach(number => {
                    newContact.phone.push(number);
                });
                question = "street";
                console.log("Katuosoite?");
                rl.prompt();
                break;
            case "street":
                newContact.street = input; 
                question = "end";
            case "end": 
                localcontacts.push(newContact);
                saveContacts(localcontacts);
                process.exit(0);
            default: 
                break;
        }

    });
}
