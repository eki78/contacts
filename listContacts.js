
export default listContacts; 
import {readContacts} from "./dbFileIO.js";

function listContacts(contactlist) {
    const list = contactlist?contactlist:readContacts();

    list.forEach(element => {
        if(element.firstname === null) {
            console.log("Etunimi: ");
        } else {console.log(`Etunimi: ${element.firstname}`);}
        if(element.lastname === null) {
            console.log("Sukunimi: ");
        } else {console.log(`Sukunimi: ${element.lastname}`);}
        console.log(`Puhelinnumero: ${element.phone}`);
        console.log(`Sähköposti: ${element.email}`);
        if(element.street === null) {
            console.log("Katuosoite: ");
        } else {console.log(`Katuosoite: ${element.street}`);}
        if(element.notes === null) {
            console.log("Muistiinpanot: ");
        } else {console.log(`Muistiinpanot: ${element.notes}`);}
        if(element.group === null) {
            console.log("Ryhmä: ");
        } else {console.log(`Ryhmä: ${element.group}`);}
        console.log(" ");
    });
    process.exit(1);
}
